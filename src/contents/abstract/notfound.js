import React from 'react'

import { Link } from 'react-router-dom'

export default function NotFound() {
  return (
    <>
        <div className="container-fluid p-5 bg-primary text-white text-center">
          <h1>Page Not Found</h1>
        </div>
        <div className="container button-homepage">         
            <Link to='/'>
                <button type='button' className='btn btn-outline-primary'>Go To Home Page</button>
            </Link>
        </div>
    </>
  )
}