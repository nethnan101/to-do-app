import React, { useState, useContext, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import { Context } from '../user/Context';
import { BiMessageSquareDetail } from 'react-icons/bi';
import { RiDeleteBin6Line , RiEdit2Line } from 'react-icons/ri';
import Swal from 'sweetalert2';
import $ from 'jquery'; 

export default function Listpage({description}) {
	const [listdes, setListdes] = useState([]);
	const [slimit, setSlimit] = useState(10); // List by pagination 10 record
	const [soffset] = useState(0);
	const [tstatus, setStatus] = useState(1); // Set 1=All, 2=Completed, 3=Pending
	const [totalPage, setTotalPage] = useState(0);

	const { user } = useContext(Context);
	let navigate = useNavigate();

	useEffect (() => {
        const fetchDescription = async (user, navigate, slimit, soffset, tstatus) => {
            var filter_status = '';
            if(tstatus === 2) filter_status = '&completed=true';
            if(tstatus === 3) filter_status = '&completed=false';

            var pagination = '?limit='+slimit+'&skip='+soffset+filter_status;

            $('.loader').show();
			const headerRe = {
				headers: {
					Authorization: 'Bearer ' + user.token
				}
			}
            const resu = await axios.get('https://api-nodejs-todolist.herokuapp.com/task'+pagination, headerRe)
                    .then(function (response) {
                        console.log(response);
                        console.log(response.data.count);

                        setTotalPage(response.data.count)

                        return response.data.data;
                    }).catch(function (error) {
                        if (error.response) {
                            $('.loader').hide();
                            navigate('*')
                        };
                    });

            if(resu) $('.loader').hide();
            setListdes(resu);
        }
        fetchDescription(user, navigate, slimit, soffset, tstatus);        
    },[user, navigate, slimit, soffset, tstatus]);

	const deleteTask = async (id) => {
		const headerRe = {
			headers: {
				Authorization: 'Bearer ' + user.token
			}
		}

        Swal.fire({
            title: 'Are you sure?',
            text: "You want to delete this data!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then( async (result) => {
            if (result.isConfirmed) {
                $('.loader').show();
                const result = await axios.delete(`https://api-nodejs-todolist.herokuapp.com/task/${id}`, headerRe)
                            .then(function (response) {
                                return response.data.success;
                            }).catch(function (error) {
                                if (error.response) {
                                    $('.loader').hide();
                                    navigate('*')
                                };
                            });     
                if(result)
                {
                    $('.loader').hide();
                    setListdes(listdes.filter((des) => des._id !== id))
                }          
            }
        })
    }

    const updateCompleted = async (task) => {
        $('.loader').show();
        var datas = {completed: !task.completed}

        const headerRe = {
			headers: {
				Authorization: 'Bearer ' + user.token
			}
		}
        const res = await axios.put(`https://api-nodejs-todolist.herokuapp.com/task/${task._id}` ,datas, headerRe)
                    .then((res) => {
                        return res.data;
                    }).catch(function (error) {
                        if (error.response) {
                            $('.loader').hide();
                            navigate('*')
                        };
                    });
        if(res.success) {
            setListdes(
                listdes.map((des) => des._id === task._id ? { ...task, completed: res.data.completed } : des)
            )
            $('.loader').hide();
        }
    }

    const filterByCompleted = async (rstatus) => {
        if(rstatus === 1) setStatus(1);
        if(rstatus === 2) setStatus(2);
        if(rstatus === 3) setStatus(3);
    }

	const goToEdit = (_id) =>{
		navigate('/editTast/' + _id)
	}

    const goToDetail = (_id) => {
        navigate('/detailpage/' + _id)
    }

    const loadMoreData = () => {
        const rlimit = slimit + 10;
        setSlimit(rlimit);
    }

    return (
        <>
            <div className="container-fluid p-5 bg-primary text-white text-center">
				<h1>Todo List</h1>
			</div>
			<div className="container">
                <div className="row">
                    <div className="col-6">
                    <Link to='/addtask'><button className='btn btn-outline-danger'>Add Task</button></Link>
                    <Link to='/useraccount'><button className='btn btn-outline-danger'>My Account</button></Link>
                    </div>
                    <div className="col-6">
                        <span>Filter your task:</span>
                        <div className="form-check">
                            <input type="radio" className="form-check-input" id="all" name="status" onClick={() => filterByCompleted(1)}  /> 
                            <label className="form-check-label" htmlFor="all"> All </label>
                        </div>

                        <div className="form-check">
                            <input type="radio" className="form-check-input" id="completed" name="status" onClick={() => filterByCompleted(2)} /> 
                            <label className="form-check-label" htmlFor="completed"> Completed </label>
                        </div>

                        <div className="form-check">
                            <input type="radio" className="form-check-input" id="pending" name="status" onClick={() => filterByCompleted(3)} /> 
                            <label className="form-check-label" htmlFor="pending"> Pending </label>
                        </div>
                    </div>
                </div>
                

			</div>
			<div className="container">                    
                <table className="table">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Despcription</th>
                            <th>Status</th>
							<th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {listdes.length > 0 ? (
                            <>
                                {listdes.map((listdes, index) => (
                                    <tr key={index}>
										<td>{listdes.createdAt}</td>
                                        <td>{listdes.description}</td>
                                        <td>
                                            {listdes.completed ? 
                                                <>
                                                    <span title="Click me change status!" onClick={() => updateCompleted(listdes)} className='badge bg-success pointer'>Completed</span>
                                                </> 
                                                : 
                                                <>
                                                    <span title="Click me change status!" onClick={() => updateCompleted(listdes)} className='badge bg-warning pointer'>Pending</span>
                                                </>
                                            }
                                        </td>
										<td>
                                            { <RiEdit2Line onClick={() => goToEdit(listdes._id)} className="pointer" />} &nbsp;&nbsp;
                                            { <RiDeleteBin6Line onClick={() => deleteTask(listdes._id)} className="pointer"/>} &nbsp;&nbsp;
                                            { <BiMessageSquareDetail onClick={() => goToDetail(listdes._id)} className="pointer"/>}
                                        </td>
                                    </tr>

                                ))}
                            </>
                        ) : (
                            <>
                                <tr>
                                    <td colSpan="4">There are no record!</td>
                                </tr>
                            </>
                        ) }
                    </tbody>
                </table>
                {slimit <= totalPage && 
                    <>
                        <div className="d-grid gap-3">
                            <br></br>
                            <button type='button' className='btn btn-outline-primary btn-block' onClick={() => loadMoreData()}> Load more data </button>
                        </div>
                    </>
                }

                
            </div>
        </>
    )
}
