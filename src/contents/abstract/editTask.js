import axios from 'axios';
import React, { useState, useContext, useEffect } from 'react';
import { Link ,useNavigate, useParams } from 'react-router-dom';
import { Context } from '../user/Context';
import $ from 'jquery'; 

export default function EditTask() {
    const [description, setDescription] = useState('');
    const { user } = useContext(Context);
    let navigate = useNavigate();
    let { id } = useParams();

    const onUpdate = async (e) => {
        e.preventDefault();
        if(!description) {
            alert('Enter your task')
            return
        }
        
        $('.loader').show();
        var newTask = await UpdateTask({description}, id);
        if(newTask){
            $('.loader').hide();
            navigate('/listpage')
        }
    }

    const UpdateTask = async (datas, id) => {
        const headerRe = {
			headers: {
				Authorization: 'Bearer ' + user.token
			}
		}
        const res = await axios.put(`https://api-nodejs-todolist.herokuapp.com/task/${id}` ,datas, headerRe)
                    .then((res) => {
                        return res.data;
                    }).catch(function (error) {
                        if (error.response) {
                            $('.loader').hide();
                            navigate('*')
                        };
                    });
        return res;
    }

    useEffect (() => {
        $('.loader').show();
        const fetchTaskById = async (id, user, navigate) => {
            const headerRe = {
                headers: {
                    Authorization: 'Bearer ' + user.token
                }
            }
            const res = await axios.get(`https://api-nodejs-todolist.herokuapp.com/task/${id}`, headerRe)
                .then((res) => {
                    return res.data;
                }).catch(function (error) {
                    if (error.response) {
                        $('.loader').hide();
                        navigate('*')
                    };
                })

            if(res.data.description) 
            {
                setDescription(res.data.description)
                $('.loader').hide();
            }
        }
        fetchTaskById(id, user, navigate)
    },[id, user, navigate])

  return (
    <>
        <div className="container-fluid p-5 bg-primary text-white text-center">
            <h1>Update Your Task</h1>
        </div>
        <div className='containers'>
            <form className='add-form' onSubmit={onUpdate}>
                <div className='form-control'>
                    <label>Description</label>
                    <input
                        type='text'
                        placeholder='Description'
                        value={description} 
                        onChange={(e) => setDescription(e.target.value)} 
                    />
                </div>
                <input type='submit' value='Add' className='btn btn-block '/>
                <Link to='/listpage'><button className='btn btn-outline-danger'>Cancel</button></Link>
            </form>
        </div>
    </>
  )
}
