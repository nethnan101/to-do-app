import React, { useState, useContext } from 'react'
import axios from 'axios';
import { Context } from '../user/Context';
import {Link , useNavigate } from 'react-router-dom';
import $ from 'jquery'; 

const Addtask = ({ onAdd }) => {
    const [description, setDescription] = useState('');
    const { user } = useContext(Context);
    let navigate = useNavigate();

    const onSubmit = async (e) => {
        e.preventDefault()
        if(!description) {
            alert('Enter your task')
            return
        }
        $('.loader').show();
        var onAdd = await addTask ({ description});
        if(onAdd.success) {
            setDescription('');
            $('.loader').hide();
            navigate('/listpage');
        }
    }

    const addTask = async (datas) => {
        const headerRe = {
			headers: {
				Authorization: 'Bearer ' + user.token
			}
		}

		const res = await axios.post('https://api-nodejs-todolist.herokuapp.com/task', datas ,headerRe)
                    .then(function (response) {
                        return response.data;
                    })
        return res;
    }

    return (
        <>
            <div className="container-fluid p-5 bg-primary text-white text-center">
				<h1>Add Your Task</h1>
			</div>
            <div className='containers'>
            <form className='add-form' onSubmit={onSubmit}>
                <div className='form-control'>
                    <label>Description</label>
                    <input
                        autoFocus
                        type='text'
                        placeholder='Description'
                        value={description} 
                        onChange={(e) => setDescription(e.target.value)} 
                    />
                </div>
                <input type='submit' value='Add' className='btn btn-block '/>
                <Link to='/listpage'><button className='btn btn-outline-danger'>Cancel</button></Link>
            </form>
        </div>
        </>
        
    )
}

export default Addtask