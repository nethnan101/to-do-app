import React from 'react'

import { Link } from 'react-router-dom'

export default function Homepage() {
  return (
    <>
        <div className="container-fluid p-5 bg-primary text-white text-center">
            <h1>To Do Application</h1>
            <p>Manage your personal task!</p> 
        </div>
        
        <div className="container mt-5">
            <div className="row">
                <div className="col-sm-6">
                    <div className="d-grid gap-3">
                        <Link to='/login' className='btn btn-outline-primary btn-block'> Login </Link>
                    </div>

                </div>
                <div className="col-sm-6">
                    <div className="d-grid gap-3">
                        <Link to='/register' className='btn btn-outline-success btn-block'>Register </Link>
                    </div>
                </div>
            </div>
        </div>
    </>
  )
}