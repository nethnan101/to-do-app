import React, { useContext, useEffect, useState} from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { Context } from '../user/Context';
import axios from 'axios';
import $ from 'jquery'; 

export default function EditUser() {
    const [name, setName] = useState('');
	const [age, setAge] = useState('');
	const { user, dispatch} = useContext(Context);
	let navigate = useNavigate();

	const onEditUser = async (e) => {
        e.preventDefault();
        if(!name){
            alert('Please enter your gmail!');
            return;
        }

        if(!age){
            alert('Please enter your age!');
            return;
        }

        $('.loader').show();
        var updateNewUser = await updateInfoUser({name, age});
        if(updateNewUser)
        {          
            $('.loader').hide();
            user.user.name = updateNewUser.data.name;
            user.user.age = updateNewUser.data.age;
            dispatch({payload: user });
            navigate('/useraccount')
        }
    }
    const updateInfoUser = async (datas) => {
        const headerRe = {
			headers: {
				Authorization: 'Bearer ' + user.token
			}
		}
        const res = await axios.put('https://api-nodejs-todolist.herokuapp.com/user/me', datas, headerRe)
                    .then((res) => {
                        return res.data;
                    }).catch(function (error) {
                        if (error.response) {
                            $('.loader').hide();
                            navigate('*')
                        };
                    });
        return res;
    }

    useEffect (() => {
        $('.loader').show();
        const fetchUser = async (user, navigate) => {
            const headerRe = {
                headers: {
                    Authorization: 'Bearer ' + user.token
                }
            }
            const res = await axios.get('https://api-nodejs-todolist.herokuapp.com/user/me', headerRe)
                        .then((res) => {
                            return res.data;
                        }).catch(function (error) {
                            if (error.response) {
                                $('.loader').hide();
                                navigate('*')
                            };
                        });
            if(res) {
                $('.loader').hide();
                setName(res.name);
                setAge(res.age);
            }
        }
        fetchUser(user, navigate)
    },[user, navigate])

	return (
		<>
            <div className="container-fluid p-5 bg-primary text-white text-center">
                <h1>Edit Your Account</h1>
            </div>
			<div className="container rounded">
                <form onSubmit={onEditUser}>
                    <div className="mb-3 mt-3">
                        <label htmlFor="name">Name:</label>
                        <input type="text" className="form-control" id="name" placeholder="Enter name" name="name" value={name} onChange={ (e) => setName(e.target.value) } />
                    </div>
                    <div className="mb-3 mt-3">
                        <label htmlFor="age">Age:</label>
                        <input type="number" className="form-control" id="age" placeholder="Enter age" name="age" value={age} onChange={ (e) => setAge(e.target.value) } />
                    </div>
                    <button type="submit" className="btn btn-primary">Save</button> <span> </span>
                    <Link to='/useraccount'><button className="btn btn-outline-primary">Cencel</button></Link>
                </form>
			</div>
		</>
	)
}
