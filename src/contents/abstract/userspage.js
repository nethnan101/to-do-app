import React, { useContext } from 'react';
import { Link, useNavigate} from 'react-router-dom';
import { Context } from '../user/Context';
import axios from 'axios';

export default function Userspage() {

	const { user, dispatch} = useContext(Context);
	let navigate = useNavigate();

	const Logout = async (datas) => {
		const headerRe = {
			headers: {
				Authorization: 'Bearer ' + user.token
			}
		}
		const res = await axios.post('https://api-nodejs-todolist.herokuapp.com/user/logout', "" ,headerRe)
			.then(function (response) {
				return response.data;
			})
		
		if(res.success){
			dispatch({ type: "LOGOUT", payload: {} });
			navigate('/');
		}
	}

	const goToEditUser = (_id) => {
		navigate('/edituser/' + _id)
	}

	return (
		<>
			<div className="container-fluid p-5 bg-primary text-white text-center">
				<h1>Your Account</h1>
			</div>
			<div className='container'>
				<Link to='/addtask' className=''><button className='btn btn-primary'>Add ToDo</button></Link>
				<Link to='/listpage' className=''><button className='btn btn-primary'>List ToDo</button></Link>
				<Link onClick={Logout} to=''><button className='btn btn-outline-danger'>Log out</button></Link>
			</div>
			<div className="container border border-danger rounded">
				<table className="table table-hover">
					<tbody>
						<tr>
							<th scope="row">Name:</th>
							<td>{user.user.name}</td>
						</tr>
						<tr>
							<th scope="row">Email</th>
							<td>{user.user.email}</td>
						</tr>
						<tr>
							<th scope="row">Age</th>
							<td>{user.user.age}</td>
						</tr>
						<button onClick={() => goToEditUser(user.user._id)} className='btn btn-outline-primary'>Edit Profile</button>
					</tbody>
				</table>
			</div>
		</>
	)
}
