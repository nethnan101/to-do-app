import React, {useContext, useEffect, useState} from 'react';
import { Link, useParams, useNavigate } from 'react-router-dom';
import axios from 'axios';
import { Context } from '../user/Context';
import $ from 'jquery'; 

export default function Detailpage() {
    const [ description, setDescription ] = useState('');
    const [ createdAt, setCreatedAt ] = useState('');
    const [ tcompleted, setCompleted ] = useState('');
    const { user } = useContext(Context);
    const { id } = useParams();
    const navigate = useNavigate();

    useEffect (() => {
        const fetchTaskById = async (id, user, navigate) => {
            $('.loader').show();

            const headerRe = {
                headers: {
                    Authorization: 'Bearer ' + user.token
                }
            }
            const res = await axios.get(`https://api-nodejs-todolist.herokuapp.com/task/${id}`, headerRe)
                        .then((res) => {
                            console.log(res);
                            return res.data;
                        }).catch(function (error) {
                            if (error.response) {
                                $('.loader').hide();
                                navigate('*')
                            };
                        });
            if(res){
                $('.loader').hide();
                setDescription(res.data.description)
                setCreatedAt(res.data.createdAt)
                setCompleted(res.data.completed)
            }
        }

        fetchTaskById(id, user, navigate)
    },[id, user, navigate])

  return (
    <>
        <div className="container-fluid p-5 bg-primary text-white text-center">
            <h1>Detail your task</h1>
        </div>
        <div className="container">
            <Link to='/listpage'><button className='btn btn-outline-danger'>Go Back</button></Link>
            <div className="border border-info rounded">
                <div className="container">
                    <h4>Your description</h4>
                    <p>Description: <b>{description}</b></p>
                    <p>Status: 
                        {tcompleted ? 
                            <>
                                <span className='badge bg-success'>Completed</span>
                            </> 
                            : 
                            <>
                                <span className='badge bg-warning'>Pending</span>
                            </>
                        }
                        </p>
                    <p>Created At: <b>{createdAt}</b></p>
                </div>
            </div>
        </div>
    </>
  )
}
